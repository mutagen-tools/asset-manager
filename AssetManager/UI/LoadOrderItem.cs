﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AssetManager.Annotations;
using AssetManager.UI.Settings;
using Mutagen.Bethesda.Environments;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Assets.Managers;

namespace AssetManager.UI;

public class LoadOrderItem : INotifyPropertyChanged {
    public LoadOrderItem(ModKey modKey, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment) {
        ModKey = modKey;
        Assets = new PluginManager(environment, modKey.FileName);
        
        PropertyChanged += OnPropertyChanged;
    }

    private async void OnPropertyChanged(object? sender, PropertyChangedEventArgs e) {
        switch (e.PropertyName) {
            case nameof(IsChecked):
                if (!_isLoaded) {
                    AssetManagerWindow.Instance.DisableUI(this, true);
                    AssetManagerWindow.Instance.LoadSpinner.LoadSpinnerText = $"Loading {ModKey.FileName}";

                    await Task.Run(() => {
                        Assets.SkipResolvedAssets = !AssetManagerSettings.Data.ProcessComplexPluginFiles;
                        Assets.ParseAssets();
                        _isLoaded = true;
                    });

                    AssetManagerWindow.Instance.DisableUI(this, false);
                }
                AssetManagerWindow.Instance.UpdateAssets();

                break;
        }
    }

    public ModKey ModKey { get; set; }
    public bool IsChecked {
        get => _isChecked;
        set {
            _isChecked = value;
            OnPropertyChanged(nameof(IsChecked));
        }
    }

    private bool _isChecked;
    private bool _isLoaded;
    
    public bool RequiresAssets {
        get => Assets.RequiresAssets;
        set => Assets.RequiresAssets = value;
    }
    public PluginManager Assets { get; }

    public event PropertyChangedEventHandler? PropertyChanged;
    [NotifyPropertyChangedInvocator]
    private void OnPropertyChanged([CallerMemberName] string? propertyName = null) {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
