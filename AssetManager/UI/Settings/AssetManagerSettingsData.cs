﻿using System.Collections.Generic;
using Mutagen.Bethesda.WPF.Reflection.Attributes;
using MutagenLibrary.WPF.Json.Editor;
using MutagenLibrary.WPF.Json.Presets;
namespace AssetManager.UI.Settings;

public class AssetManagerSettingsEditor : JsonEditor<AssetManagerSettingsData> {}

public class AssetManagerConfigurator : BasicPresetsConfigurator<AssetManagerSettingsData, AssetManagerSettings, AssetManagerSettingsEditor> {
    public override string Header { get; set; } = "Settings";
}

public class AssetManagerSettings : BasicPresetManager<AssetManagerSettingsData, AssetManagerSettings> {
    protected override string PresetsDirectoryName => "Settings";
    public override string DefaultName => "Settings";

    protected override AssetManagerSettingsData DefaultData => new() {
        ProcessComplexPluginFiles = true,
        FavoriteDirectory = new List<string>(),
    };
}

public class AssetManagerSettingsData {
    public AssetManagerSettingsData() {}
    
    [Tooltip("Folders that can quickly be selected in the directory pane.")]
    public List<string> FavoriteDirectory = new();

    [Tooltip("With this enabled, complex files like voice lines or face gen files will be included in the search.")]
    public bool ProcessComplexPluginFiles = true;
}
