﻿using MutagenLibrary.Assets;

namespace AssetManager.UI; 

public class AssetTypeItem {
    public AssetTypeItem(AssetType assetType, bool isChecked) {
        AssetType = assetType;
        IsChecked = isChecked;
    }

    public AssetType AssetType { get; }
    public bool IsChecked { get; set; }
}