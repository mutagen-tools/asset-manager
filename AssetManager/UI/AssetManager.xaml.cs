﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Effects;
using AssetManager.UI.Settings;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Environments;
using Mutagen.Bethesda.Environments.DI;
using Mutagen.Bethesda.Plugins.Order.DI;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Assets;
using MutagenLibrary.Assets.Managers;
using MutagenLibrary.Files;
using MutagenLibrary.WPF.CustomWindows;
using ReactiveUI;
using Syncfusion.SfSkinManager;
using Button = System.Windows.Controls.Button;
using Clipboard = System.Windows.Clipboard;
using MessageBox = System.Windows.MessageBox;

//This is a mess
namespace AssetManager.UI;

public partial class AssetManagerWindow {
    #pragma warning disable CS8618
    public static AssetManagerWindow Instance;
    #pragma warning restore CS8618

    private static readonly ObservableCollection<object> LoadingObjects = new();
    private static readonly BlurEffect Blur = new() { Radius = 10 };

    public const string DeletionFolder = "_DELETE_";
    private const char SearchSeparator = '*';
    
    public ObservableCollection<AssetTypeItem> AssetTypes { get; } = new();
    public ObservableCollection<DirectoryItem> Directories { get; } = new();
    public ObservableCollection<LoadOrderItem> LoadOrder { get; } = new();
    public ObservableCollection<Asset> LoadOrderAssets { get; set; } = new();
    public ObservableCollection<Asset> DirectoryAssets { get; set; } = new();
    public int SelectedLoadOrderTab { get; set; }
    public int SelectedDirectoryTab { get; set; }
    public ICommand OpenSettings { get; }

    private IGameEnvironment<ISkyrimMod, ISkyrimModGetter> _environment;

    private readonly AssetManagerList _loadOrderAssets = new();
    private readonly AssetManagerList _directoryAssets = new();

    private bool _loaded;
        
    // ReSharper disable once MemberCanBePrivate.Global
    public static readonly RoutedUICommand GenericCommand = new("Some Command", "SomeCommand", typeof(AssetManagerWindow));

    public AssetManagerWindow() {
        var pathProvider = new PluginListingsPathProvider(new GameReleaseInjection(GameRelease.SkyrimSE));
        if (!File.Exists(pathProvider.Path)) {
            MessageBox.Show($"Make sure {pathProvider.Path} exists.");
            throw new FileNotFoundException(pathProvider.Path);
        }

        _environment = GameEnvironment.Typical.Skyrim(SkyrimRelease.SkyrimSE);
        SfSkinManager.ApplyStylesOnApplication = true;
        App.UpdateTheme(this);
            
        InitializeComponent();
            
        InitializeEnabledAssetTypes();
        InitializeLoadOrder();

        Instance = this;
        DataContext = this;

        OpenSettings = ReactiveCommand.Create(() => {
            new SettingsWindow().ShowDialog();
            
            UpdateFavoriteFolders();
        });

        UpdateFavoriteFolders();
    }

    private void UpdateFavoriteFolders() {
        DirectoryDataGrid.ContextMenu ??= new ContextMenu();
        AssetManagerSettings.Data.FavoriteDirectory.RemoveAll(d => !Directory.Exists(d));
        
        //Remove existing menu items
        while (DirectoryDataGrid.ContextMenu.Items.Count > 1) DirectoryDataGrid.ContextMenu.Items.RemoveAt(1);
        
        //Add separator
        if (AssetManagerSettings.Data.FavoriteDirectory.Any()) DirectoryDataGrid.ContextMenu.Items.Add(new Separator());
        
        //Add menu item for each folder
        foreach (var folder in AssetManagerSettings.Data.FavoriteDirectory) {
            var menuItem = new MenuItem { Header = $"Add: {folder}" };
            menuItem.Click += AddDirectory;
            
            DirectoryDataGrid.ContextMenu.Items.Add(menuItem);
        }
    }

    private void AssetManager_OnLoaded(object sender, RoutedEventArgs e) => _loaded = true;

    private IEnumerable<AssetType> GetEnabledAssetTypes() => AssetTypes.Where(item => item.IsChecked).Select(item => item.AssetType);

    private void InitializeEnabledAssetTypes() {
        foreach (var assetType in Enum.GetValues<AssetType>().Skip(1)) {
            AssetTypes.Add(new AssetTypeItem(assetType, false));
        }
    }

    private void InitializeLoadOrder() {
        foreach (var mod in _environment.LoadOrder.ListedOrder) {
            LoadOrder.Add(new LoadOrderItem(mod.ModKey, _environment));
        }
    }

    private bool _runningUpdate;
    private readonly System.Threading.EventWaitHandle _waitHandle = new System.Threading.AutoResetEvent(false);
    
    public async void UpdateAssets() {
        if (_runningUpdate) return;
        _runningUpdate = true;
        
        DisableUI(this, true);
        
        LoadingObjects.CollectionChanged += LoadingObjectsOnCollectionChanged;
        void LoadingObjectsOnCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e) {
            if (LoadingObjects.Count == 1) {
                _waitHandle.Set();
            }
        }

        await Task.Run(() => {
            //Wait until all other processes have completed
            if (LoadingObjects.Count > 1) _waitHandle.WaitOne();
            
            Dispatcher.Invoke(() => {
                LoadSpinner.LoadSpinnerText = "Updating assets";
                
                //Update select all button state
                UpdateAllDirectoriesButtonState();
                UpdateAllAssetTypesButtonState();
                UpdateAllLoadOrderButtonState();
            });
            
            //Directory Assets
            _directoryAssets.Clear();
            foreach (var directory in Directories.Where(dir => dir.IsChecked)) {
                _directoryAssets.Add(directory.Assets);
            }
            
            //Load Order Assets
            _loadOrderAssets.Clear();
            foreach (var plugin in LoadOrder.Where(plugin => plugin.IsChecked)) {
                _loadOrderAssets.Add(plugin.Assets);
            }
            foreach (var directory in Directories.Where(dir => dir.IsChecked)) {
                _loadOrderAssets.Add(directory.Nif);
            }
        
            UpdateDirectoryAssets();
            UpdateLoadOrderAssets();
        });
        
        DisableUI(this, false);
        _runningUpdate = false;

        LoadingObjects.CollectionChanged -= LoadingObjectsOnCollectionChanged;
    }

    private void UpdateAssets(object sender, RoutedEventArgs e) {
        UpdateAssets();
    }
    
    private async void MarkDirectoryAssetsForDeletion(object sender, ExecutedRoutedEventArgs e) {
        DisableUI(this, true);

        var filesToMove = new List<Asset>();
        var dirtyAssetManagers = new HashSet<MutagenLibrary.Assets.Managers.AssetManager>();
        foreach (var selectedItem in DirectoryAssetsList.SelectedItems) {
            var asset = (Asset) selectedItem;
            if (asset == null) continue;

            await Task.Run(() => {
                filesToMove.Add(asset);
                var assetType = AssetTypeLibrary.GetAssetType(asset.Path);
                foreach (var assetManager in _directoryAssets.GetUsers(asset, assetType)) {
                    var path = Path.Combine(assetManager.Name, asset.Path);

                    if (!File.Exists(path)) continue;

                    var outputFileInfo = new FileInfo(Path.Combine(assetManager.Name, DeletionFolder, asset.Path));
                    outputFileInfo.Directory?.Create();

                    Dispatcher.Invoke(() => LoadSpinner.LoadSpinnerText = $"Moving {Path.GetFileName(path)}");
                    File.Move(path, outputFileInfo.FullName, true);
                    assetManager.RemoveAsset(asset);
                    dirtyAssetManagers.Add(assetManager);
                }
            });
        }

        LoadSpinner.LoadSpinnerText = "Finalizing";

        await Task.Run(() => {
            foreach (var assetManager in dirtyAssetManagers) {
                assetManager.SaveAssetCache();
            }
        });
        
        foreach (var file in filesToMove) {
            DirectoryAssets.Remove(file);
        }
        
        UpdateDirectoryAssets();

        DisableUI(this, false);
    }
        
    private void HasSelectedMultipleDirectoryAssets(object sender, CanExecuteRoutedEventArgs e) {
        e.CanExecute = DirectoryAssetsList.SelectedItems.Count > 0;
    }

    private void CopySelectedDirectoryAssets(object sender, ExecutedRoutedEventArgs e) {
        var sb = new StringBuilder();
        foreach (var item in DirectoryAssetsList.SelectedItems) {
            sb.AppendLine(((Asset) item).Path);
        }

        var copy = sb.ToString();
        if (copy.Length != 0) Clipboard.SetText(copy);
    }

    private void CopySelectedLoadOrderAssets(object sender, ExecutedRoutedEventArgs e) {
        var sb = new StringBuilder();
        foreach (var item in LoadOrderAssetsList.SelectedItems) {
            sb.AppendLine(((Asset) item).Path);
        }

        var copy = sb.ToString();
        if (copy.Length != 0) Clipboard.SetText(copy);
    }

    private void HasSelectedOneDirectoryAsset(object sender, CanExecuteRoutedEventArgs e) {
        e.CanExecute = DirectoryAssetsList.SelectedItems.Count == 1;
    }
        
    private void HasSelectedOneLoadOrderAsset(object sender, CanExecuteRoutedEventArgs e) {
        e.CanExecute = LoadOrderAssetsList.SelectedItems.Count == 1;
    }
        
    private void HasSelectedMultipleLoadOrderAssets(object sender, CanExecuteRoutedEventArgs e) {
        e.CanExecute = LoadOrderAssetsList.SelectedItems.Count > 0;
    }
        
    private void AddAssetDirectory(object sender, ExecutedRoutedEventArgs e) {
        var dialog = new FolderBrowserDialog();
        if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
        if (Directories.Any(directory => directory.Path == dialog.SelectedPath)) return;

        DirectoryHintMessage.Visibility = Visibility.Collapsed;
        Directories.Add(new DirectoryItem(dialog.SelectedPath) {ParseAssets = true, IsChecked = true});
        UpdateAssets();
    }
    
    private void AddDirectory(object sender, RoutedEventArgs e) {
        var directory = (string) ((MenuItem) sender).Header;

        if (Directories.Any(d => d.Path == directory)) {
            DirectoryDataGrid.ContextMenu?.Items.Remove(sender);
            return;
        }
        DirectoryDataGrid.ContextMenu?.Items.Remove(sender);
        Directories.Add(new DirectoryItem(directory) {ParseAssets = true, IsChecked = true});
        DirectoryHintMessage.Visibility = Visibility.Collapsed;
        UpdateAssets();
    }
    
    private void RefreshAssetDirectory(object sender, RoutedEventArgs e) {
        var dir = (DirectoryItem) ((Button) sender).DataContext;
        dir.Update();
        
        UpdateAssets();
    }

    private void DirectoryAssetsSearchChanged(object sender, TextChangedEventArgs e) {
        CollectionViewSource.GetDefaultView(DirectoryAssetsList.ItemsSource).Refresh();
    }
        
    private bool DirectoryAssetsFilter(object item) {
        if (string.IsNullOrEmpty(DirectoryAssetsSearch.Text)) return true;

        if (item is not Asset s) return false;
        
        var mustIncludeSubstrings = DirectoryAssetsSearch.Text.Split(SearchSeparator);
        return mustIncludeSubstrings.All(sub => s.Path.Contains(sub, StringComparison.OrdinalIgnoreCase));
    }

    private void LoadOrderAssetsSearchChanged(object sender, TextChangedEventArgs e) {
        CollectionViewSource.GetDefaultView(LoadOrderAssetsList.ItemsSource).Refresh();
    }
        
    private bool LoadOrderAssetsFilter(object item) {
        if(string.IsNullOrEmpty(LoadOrderAssetsSearch.Text)) return true;
        
        if (item is not Asset s) return false;

        var mustIncludeSubstrings = LoadOrderAssetsSearch.Text.Split(SearchSeparator);
        return mustIncludeSubstrings.All(sub => s.Path.Contains(sub, StringComparison.OrdinalIgnoreCase));
    }

    private void ShowDirectoryAssetUsages(object sender, ExecutedRoutedEventArgs e) {
        var asset = (Asset) DirectoryAssetsList.SelectedItems[0]!;
        
        var usageWindow = new TableWindow(
            asset.Path,
            new List<(IReadOnlyList<string> columns, List<List<string>> rows)> {(
                    new []{"Location"},
                    new List<List<string>> {_directoryAssets.GetUsages(asset).Select(a => a.ToString()).ToList()}
            )}) {
            Owner = this
        };
        usageWindow.Show();
    }

    [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
    private void ShowLoadOrderAssetUsages(object sender, ExecutedRoutedEventArgs e) {
        var asset = (Asset) LoadOrderAssetsList.SelectedItems[0]!;

        var usages = _loadOrderAssets.GetUsages(asset);

        var files = new List<string>();
        var records = new List<List<string>>();
        foreach (var usage in usages) {
            if (usage[1] == ':') {
                files.Add(usage);
            } else {
                var recordEntry = usage.Split(PluginManager.Separator);
                for (var i = 0; i < recordEntry.Length; i++) {
                    if (records.Count <= i) {
                        records.Add(new List<string>());
                    }
                    records[i].Add(recordEntry[i]);
                }
            }
        }
        
        var grids = new List<(IReadOnlyList<string> columns, List<List<string>> rows)>();
        if (files.Count > 0) {
            grids.Add((new[] { "Location" }, new List<List<string>>{files}));
        }
        if (records.Count > 0) {
            grids.Add((new[] { "FormID", "EditorID" }, records));
        }
        if (grids.Count == 0) return;
        
        var usageMenu = new TableWindow(asset.Path, grids) { Owner = this };
        usageMenu.Show();
    }

    private void UpdateLoadOrderAssets() {
        var assets = new List<Asset>();
        switch (SelectedLoadOrderTab) {
            case 0:
                //Orphaned
                assets = _loadOrderAssets.GetMissingAssets(_directoryAssets, GetEnabledAssetTypes());
                break;
            case 1:
                //All
                assets = _loadOrderAssets.GetAllAssets(GetEnabledAssetTypes());
                break;
        }
        
        LoadOrderAssets = new ObservableCollection<Asset>(assets);

        if (!_loaded) return;

        Dispatcher.Invoke(() => {
            LoadOrderAssetsList.GetBindingExpression(ItemsControl.ItemsSourceProperty)?.UpdateTarget();
            LoadOrderAssetsCount.GetBindingExpression(ContentProperty)?.UpdateTarget();

            var view = (CollectionView) CollectionViewSource.GetDefaultView(LoadOrderAssetsList.ItemsSource);
            view.Filter = LoadOrderAssetsFilter;
        });
    }
        
    private void UpdateLoadOrderAssets(object sender, RoutedEventArgs e) {
        UpdateLoadOrderAssets();
    }
        
    private void UpdateDirectoryAssets() {
        var assets = new List<Asset>();
        switch (SelectedDirectoryTab) {
            case 0:
                //Orphaned
                assets = _directoryAssets.GetMissingAssets(_loadOrderAssets, GetEnabledAssetTypes());
                break;
            case 1:
                //All
                assets = _directoryAssets.GetAllAssets(GetEnabledAssetTypes());
                break;
        }

        DirectoryAssets = new ObservableCollection<Asset>(assets);
        if (!_loaded) return;
        
        Dispatcher.Invoke(() => {
            DirectoryAssetsSize.Content = FileLibrary.GetFileSize(assets.Sum(asset => asset.Size));
            DirectoryAssetsList.GetBindingExpression(ItemsControl.ItemsSourceProperty)?.UpdateTarget();
            DirectoryAssetsCount.GetBindingExpression(ContentProperty)?.UpdateTarget();
                
            var view = (CollectionView) CollectionViewSource.GetDefaultView(DirectoryAssetsList.ItemsSource);
            view.Filter = DirectoryAssetsFilter;
        });
    }
        
    private void UpdateDirectoryAssets(object sender, RoutedEventArgs e) {
        UpdateDirectoryAssets();
    }
    
    private void RefreshAllDirectories(object sender, RoutedEventArgs e) {
        RefreshAllDirectories();
    }
    
    private void RefreshAllDirectories() {
        foreach (var dir in Directories) dir.Update();
        
        UpdateAssets();
    }
    
    private void RefreshLoadOrder(object sender, RoutedEventArgs e) {
        RefreshLoadOrder();
    }
    
    private void RefreshLoadOrder() {
        _environment = GameEnvironment.Typical.Skyrim(SkyrimRelease.SkyrimSE);
        
        var selectedMods = LoadOrder.Where(i => i.IsChecked).Select(i => i.ModKey).ToList();
        var requiredMods = LoadOrder.Where(i => i.RequiresAssets).Select(i => i.ModKey).ToList();

        LoadOrder.Clear();
        foreach (var mod in _environment.LoadOrder.ListedOrder) {
            var newItem = new LoadOrderItem(mod.ModKey, _environment);
            LoadOrder.Add(newItem);

            if (selectedMods.Contains(mod.ModKey)) newItem.IsChecked = true;
            if (requiredMods.Contains(mod.ModKey)) newItem.RequiresAssets = true;
        }
    }

    private void OpenFile(object sender, ExecutedRoutedEventArgs e) {
        foreach (var selectedItem in DirectoryAssetsList.SelectedItems) {
            var asset = (Asset) selectedItem;

            var assetType = AssetTypeLibrary.GetAssetType(asset.Path);
            foreach (var assetManager in _directoryAssets.GetUsers(asset, assetType)) {
                var path = Path.Combine(assetManager.Name, asset.Path);
                if (!File.Exists(path)) continue;

                using var process = new Process {
                    StartInfo = new ProcessStartInfo {
                        FileName = "explorer",
                        Arguments = $"\"{path}\""
                    }
                };
                process.Start();
            }
        }
    }
    
    private void ToggleAllDirectories(object sender, RoutedEventArgs e) {
        //If any directory isn't checked, enable all, otherwise disable all
        var newState = Directories.Any(dir => !dir.IsChecked);

        foreach (var dir in Directories) {
            dir.IsChecked = newState;
        }
            
        DirectoryDataGrid.Items.Refresh();
            
        UpdateAssets();
    }

    private void ToggleAllAssetTypes(object sender, RoutedEventArgs e) {
        //If any asset type isn't checked, enable all, otherwise disable all
        var newState = AssetTypes.Any(assetType => !assetType.IsChecked);

        foreach (var assetType in AssetTypes) {
            assetType.IsChecked = newState;
        }
            
        AssetTypeView.Items.Refresh();
            
        UpdateAssets();
    }

    private void ToggleLoadOrder(object sender, RoutedEventArgs e) {
        //If any mod isn't checked, enable all, otherwise disable all
        var newState = LoadOrder.Any(plugin => !plugin.IsChecked);

        foreach (var plugin in LoadOrder) {
            plugin.IsChecked = newState;
        }
            
        LoadOrderList.Items.Refresh();
            
        UpdateAssets();
    }
        
    private void UpdateAllDirectoriesButtonState() {
        var newStatus = Directories.All(dir => dir.IsChecked) && Directories.Count > 0;
        if (newStatus == ToggleAllDirectoriesCheckBox.IsChecked) return;
            
        if (newStatus) {
            ToggleAllDirectoriesCheckBox.Checked -= ToggleAllDirectories;
            ToggleAllDirectoriesCheckBox.IsChecked = true;
            ToggleAllDirectoriesCheckBox.Checked += ToggleAllDirectories;
        } else {
            ToggleAllDirectoriesCheckBox.Unchecked -= ToggleAllDirectories;
            ToggleAllDirectoriesCheckBox.IsChecked = false;
            ToggleAllDirectoriesCheckBox.Unchecked += ToggleAllDirectories;
        }
    }
        
    private void UpdateAllAssetTypesButtonState() {
        var newStatus = AssetTypes.All(dir => dir.IsChecked);
        if (newStatus == ToggleAllAssetTypesCheckBox.IsChecked) return;
            
        if (newStatus) {
            ToggleAllAssetTypesCheckBox.Checked -= ToggleAllAssetTypes;
            ToggleAllAssetTypesCheckBox.IsChecked = true;
            ToggleAllAssetTypesCheckBox.Checked += ToggleAllAssetTypes;
        } else {
            ToggleAllAssetTypesCheckBox.Unchecked -= ToggleAllAssetTypes;
            ToggleAllAssetTypesCheckBox.IsChecked = false;
            ToggleAllAssetTypesCheckBox.Unchecked += ToggleAllAssetTypes;
        }
    }
        
    private void UpdateAllLoadOrderButtonState() {
        var newStatus = LoadOrder.All(dir => dir.IsChecked);
        if (newStatus == ToggleLoadOrderCheckBox.IsChecked) return;
            
        if (newStatus) {
            ToggleLoadOrderCheckBox.Checked -= ToggleLoadOrder;
            ToggleLoadOrderCheckBox.IsChecked = true;
            ToggleLoadOrderCheckBox.Checked += ToggleLoadOrder;
        } else {
            ToggleLoadOrderCheckBox.Unchecked -= ToggleLoadOrder;
            ToggleLoadOrderCheckBox.IsChecked = false;
            ToggleLoadOrderCheckBox.Unchecked += ToggleLoadOrder;
        }
    }
    
    public void DisableUI(object origin, bool loading) {
        if (loading) {
            LoadingObjects.Add(origin);
        } else {
            LoadingObjects.Remove(origin);
        }
        
        switch (loading || LoadingObjects.Count > 0) {
            case true:
                LoadSpinner.Visibility = Visibility.Visible;
                MainGrid.IsHitTestVisible = false;
                MainGrid.Effect = Blur;
                Menu.IsHitTestVisible = false;
                Menu.Effect = Blur;
                break;
            default:
                LoadSpinner.Visibility = Visibility.Collapsed;
                LoadSpinner.LoadSpinnerText = string.Empty;
                MainGrid.IsHitTestVisible = true;
                MainGrid.Effect = null;
                Menu.IsHitTestVisible = true;
                Menu.Effect = null;
                break;
        }
    }
}