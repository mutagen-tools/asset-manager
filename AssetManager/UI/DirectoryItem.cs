﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AssetManager.Annotations;
using MutagenLibrary.Assets.Managers;

namespace AssetManager.UI;

public sealed class DirectoryItem : INotifyPropertyChanged {
    public DirectoryItem(string path) {
        Path = path;
        Directory = System.IO.Path.GetFileName(path);

        var ignoreFolders = new List<string> { AssetManagerWindow.DeletionFolder };
        Assets = new DirectoryManager(path, ParseBSA, ignoreFolders);
        Nif = new NifAssetManager(path, ParseBSA, ignoreFolders);
        
        PropertyChanged += OnPropertyChanged;
    }

    public void Update() {
        var ignoreFolders = new List<string> { AssetManagerWindow.DeletionFolder };
        Assets = new DirectoryManager(Path, ParseBSA, ignoreFolders);
        Nif = new NifAssetManager(Path, ParseBSA, ignoreFolders);

        if (ParseAssets) {
            OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(ParseAssets)));
        }

        ParseNif = false;
        ParseBSA = false;
    }

    private async void OnPropertyChanged(object? sender, PropertyChangedEventArgs e) {
        switch (e.PropertyName) {
            case nameof(ParseAssets):
                if (_parseAssets) {
                    AssetManagerWindow.Instance.DisableUI(this, true);
                    AssetManagerWindow.Instance.LoadSpinner.LoadSpinnerText = $"Loading {Directory}";
                    
                    await Task.Run(() => Assets.ParseAssets());

                    AssetManagerWindow.Instance.DisableUI(this, false);
                }
                AssetManagerWindow.Instance.UpdateAssets();

                break;
            case nameof(ParseNif):
                if (_parseNif) {
                    AssetManagerWindow.Instance.DisableUI(this, true);
                    AssetManagerWindow.Instance.LoadSpinner.LoadSpinnerText = $"Loading {Directory} nif files";

                    await Task.Run(() => Nif.ParseAssets());

                    AssetManagerWindow.Instance.DisableUI(this, false); 
                }
                AssetManagerWindow.Instance.UpdateAssets();
                
                break;
            case nameof(ParseBSA):
                if (_parseBSA) {
                    AssetManagerWindow.Instance.DisableUI(this, true);
                    AssetManagerWindow.Instance.LoadSpinner.LoadSpinnerText = $"Loading {Directory} BSAs";
                    
                    await Task.Run(() => {
                        //Parsing all sub managers always works as the directory runs when it's loaded
                        Assets.ForceParseSubManagers();
                        
                        //Enable BSA parsing if nif didn't run yet
                        Nif.EnableBSAParsing();
                        //Otherwise parsing all sub managers should work
                        Nif.ForceParseSubManagers();
                    });

                    AssetManagerWindow.Instance.DisableUI(this, false);
                }
                AssetManagerWindow.Instance.UpdateAssets();
                
                break;
        }
    }

    public string Path { get; }

    // ReSharper disable once UnusedAutoPropertyAccessor.Global
    // ReSharper disable once MemberCanBePrivate.Global
    public string Directory { get; }

    public bool IsChecked { get; set; }

    public bool ParseAssets {
        get => _parseAssets;
        set {
            _parseAssets = value;
            OnPropertyChanged(nameof(ParseAssets));
        }
    }

    private bool _parseAssets;

    public bool ParseNif {
        get => _parseNif;
        set {
            _parseNif = value;
            OnPropertyChanged(nameof(ParseNif));
        }
    }

    private bool _parseNif;

    // ReSharper disable once MemberCanBePrivate.Global
    public bool ParseBSA {
        get => _parseBSA;
        // ReSharper disable once UnusedMember.Global
        set {
            _parseBSA = value;
            OnPropertyChanged(nameof(ParseBSA));
        }
    }

    private bool _parseBSA;

    // ReSharper disable once MemberCanBePrivate.Global
    public bool RequiresAssets {
        get => Assets.RequiresAssets;
        set => Assets.RequiresAssets = value;
    }

    public DirectoryManager Assets;
    public NifAssetManager Nif;
    public event PropertyChangedEventHandler? PropertyChanged;

    [NotifyPropertyChangedInvocator]
    private void OnPropertyChanged([CallerMemberName] string? propertyName = null) {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
